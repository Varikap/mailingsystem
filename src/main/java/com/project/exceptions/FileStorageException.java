package com.project.exceptions;


public class FileStorageException extends RuntimeException{

	private static final long serialVersionUID = -3494662688687294816L;

	public FileStorageException(String message) {
		super(message);
	}

	public FileStorageException(String message, Throwable cause) {
		super(message, cause);
	}
}
