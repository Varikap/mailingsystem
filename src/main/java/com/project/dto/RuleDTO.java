package com.project.dto;

import javax.validation.constraints.Size;

import com.project.model.Rule;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RuleDTO {
	private Long id;

	@Size(max=100, message = "Minimum 100 char")
	private String value;
	private Integer condition;
	private Integer operation;
	
	public RuleDTO (Rule r) {
		this.id = r.getId();
		this.value = r.getValue();
		this.condition = r.getCondition();
		this.operation = r.getOperation();
	}
}
