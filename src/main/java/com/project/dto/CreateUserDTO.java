package com.project.dto;


import javax.validation.constraints.Size;

import org.springframework.lang.Nullable;

import com.project.model.User;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateUserDTO {
	@Size(max=20, message = "Minimum 20 char")
	private String username;

	@Size(max=20, message = "Minimum 20 char")
	private String password;

	@Size(max=100, message = "Minimum 100 char")
	private String firstname;

	@Size(max=100, message = "Minimum 100 char")
	private String lastname;
	
	public CreateUserDTO (User u) {
		this.username = u.getUsername();
		this.password = u.getPassword();
		this.firstname = u.getFirstname();
		this.lastname = u.getLastname();
	}
}
