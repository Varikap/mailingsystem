package com.project.dto;


import javax.validation.constraints.Size;

import com.project.model.Attachment;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AttachmentDTO {
	private Long id;

	@Size(max=20, message = "Minimum 20 char")
	private String mime_type;

	@Size(max=100, message = "Minimum 100 char")
	private String name;
	private String fileDownloadUri;
	private Long size;
	
	public AttachmentDTO (Attachment a) {
		this.id = a.getId();
		this.mime_type = a.getMime_type();
		this.name = a.getName();
	}
}
