package com.project.dto;

import javax.validation.constraints.Size;

import com.project.model.Tag;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TagDTO {
	private Long id;
	@Size(max=100, message = "Minimum 100 char")
	private String name;
	
	public TagDTO (Tag t) {
		this.id = t.getId();
		this.name = t.getName();
	}
}
