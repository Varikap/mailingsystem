package com.project.dto;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.project.model.Account;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AccountDTO {
	private Long id;
	@Size(max=20, message = "Minimum 20 char")
	private String username;

	@Size(max=20, message = "Minimum 20 char")
	private String password;

	@Size(max=100, message = "Minimum 100 char")
	private String displayName;
	
	@Size(max=250, message = "Minimum 250 char")
	private String smtpAddress;

	private Integer smtpPort;

	@Size(max=250, message = "Minimum 250 char")
	private String inServerAddress;

	private Integer inServerType;
	
	public AccountDTO(Account a) {
		this.id = a.getId();
		this.username = a.getUsername();
		this.password = a.getPassword();
		this.displayName = a.getDisplayName();
		this.smtpAddress = a.getSmtpAddress();
		this.smtpPort = a.getSmtpPort();
		this.inServerAddress = a.getInServerAddress();
		this.inServerType = a.getInServerType();
	}
}
