package com.project.dto;

import javax.validation.constraints.Size;

import com.project.model.Folder;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FolderDTO {
	private Long id;

	@Size(max=100, message = "Minimum 100 char")
	private String name;

	public FolderDTO (Folder f) {
		this.id = f.getId();
		this.name = f.getName();
	}
}
