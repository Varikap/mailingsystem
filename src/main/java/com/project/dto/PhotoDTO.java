package com.project.dto;

import javax.validation.constraints.Size;

import com.project.model.Photo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PhotoDTO {
	private Long id;

	@Size(max=250, message = "Minimum 250 char")
	private String fileDownloadUri;
	private Long size;
	
	public PhotoDTO (Photo p) {
		this.id = p.getId();
	}
}
