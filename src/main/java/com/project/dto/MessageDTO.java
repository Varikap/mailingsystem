package com.project.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

import com.project.model.Message;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MessageDTO implements Serializable{
	private static final long serialVersionUID = -3942043386738157137L;
	private Long id;

	@Size(max=100, message = "Minimum 100 char")
	private String from;
	@NotNull
	@Email
	private String to;
	private String cc;
	private String bcc;
	private String dateTime;
	@Size(max=250, message = "Minimum 250 char")
	private String subject;
	private String content;
	private Boolean unread;
	private List<AttachmentDTO> files;
	
	public MessageDTO (Message m) {
		this.id = m.getId();
		this.from = m.getFrom();
		this.to =  m.getTo();
		this.cc = m.getCc();
		this.bcc = m.getBcc();
		this.dateTime = m.getDateTime().toString();
		this.subject = m.getSubject();
		this.content = m.getContent();
		this.unread = m.getUnread();
	}
}
