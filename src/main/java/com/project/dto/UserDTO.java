package com.project.dto;


import javax.validation.constraints.Size;

import com.project.model.User;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDTO {
	private Long id;

	@Size(max=20, message = "Minimum 20 char")
	private String username;

	@Size(max=20, message = "Minimum 20 char")
	private String firstname;

	@Size(max=100, message = "Minimum 100 char")
	private String lastname;
	
	public UserDTO (User u) {
		this.id = u.getId();
		this.username = u.getUsername();
		this.firstname = u.getFirstname();
		this.lastname = u.getLastname();
	}
}
