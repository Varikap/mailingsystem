package com.project.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LoginDTO {
	@NotNull
	@Size(max=20, message = "Minimum 20 char")
	private String username;
	@NotNull
	@Size(max=20, message = "Minimum 20 char")
	private String password;
}
