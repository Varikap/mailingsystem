package com.project.dto;

import java.io.Serializable;

import javax.validation.constraints.Size;

import org.springframework.lang.Nullable;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.project.model.Contact;
import com.project.model.Photo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ContactDTO implements Serializable{
	private static final long serialVersionUID = -6773627405717263866L;
	@Nullable
	private Long id;

	@Size(max=100, message = "Minimum 100 char")
	private String displayname;
	
	@Size(max=100, message = "Minimum 100 char")
	private String email;
	
	@Size(max=100, message = "Minimum 100 char")
	private String firstname;
	
	@Size(max=100, message = "Minimum 100 char")
	private String lastname;
	private String note;
	@Nullable
	private PhotoDTO photoDTO;
	
	public ContactDTO (Contact c) {
		this.id = c.getId();
		this.displayname = c.getDisplayname();
		this.email = c.getEmail();
		this.firstname = c.getFirstname();
		this.lastname = c.getLastname();
		this.note = c.getNote();
		this.photoDTO = new PhotoDTO();
		Photo p = (Photo) c.getPhotos().toArray()[0];
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("api/file/downloadFile/")
                .path(p.getPath())
                .toUriString();
		this.photoDTO.setFileDownloadUri(fileDownloadUri);
	}
}
