package com.project.utils;

import org.springframework.stereotype.Component;

@Component
public class FolderNameConverter {
	public String convert(String folderName) {
		switch (folderName.toLowerCase()) {
			case "all":
				folderName = "[Gmail]/Сва пошта";
				break;
			case "sent":
				folderName = "[Gmail]/Послате";
				break;
			case "star":
				folderName = "[Gmail]/Са звездицом";
				break;
			case "deleted":
				folderName = "[Gmail]/Отпад";
				break;
			case "important":
				folderName = "[Gmail]/Важно";
				break;
			default:
				folderName = "INBOX";
		}
		return folderName;
	}
}
