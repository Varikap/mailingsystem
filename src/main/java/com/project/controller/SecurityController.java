package com.project.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.dto.LoginDTO;
import com.project.dto.TokenDTO;
import com.project.dto.UserDTO;
import com.project.model.User;
import com.project.model.UserAuthority;
import com.project.model.enumerations.Role;
import com.project.security.TokenUtils;
import com.project.service.UserService;

@RestController
@RequestMapping(value="api")
public class SecurityController {
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private TokenUtils tokenUtils;
	
	@Autowired
	private UserService userService;
	
	@PostMapping(value="/login")
	public ResponseEntity<TokenDTO> login(@Valid @RequestBody LoginDTO loginDTO) {
		try {
			UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(
					loginDTO.getUsername(), loginDTO.getPassword());
            Authentication authentication = authenticationManager.authenticate(upToken);
            UserDetails details = userDetailsService.loadUserByUsername(loginDTO.getUsername());
            TokenDTO tokenDTO = new TokenDTO();
            String token = tokenUtils.generateToken(details);
            tokenDTO.setToken(token);
            tokenDTO.setExpiresIn(tokenUtils.getExpirationTime());
            return new ResponseEntity<TokenDTO>(tokenDTO, HttpStatus.OK);
        } catch (Exception ex) {
        	System.out.println(ex.getMessage().toString());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
	}
	
	@GetMapping(value="/loggedin")
	ResponseEntity<Object> getLoggedIn(@RequestHeader("X-Auth-Token") String token){
		User u = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
		return new ResponseEntity<Object>(new UserDTO(u), HttpStatus.OK);

	}
}
