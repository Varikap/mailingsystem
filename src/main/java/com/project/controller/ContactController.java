package com.project.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.converter.ContactConverter;
import com.project.dto.ContactDTO;
import com.project.dto.PhotoDTO;
import com.project.model.Contact;
import com.project.model.User;
import com.project.security.TokenUtils;
import com.project.service.ContactService;
import com.project.service.UserService;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value="api/contact")
@Log4j2
public class ContactController {
	@Autowired
	private ContactService contactService;
	@Autowired
	private ContactConverter contactConverter;
	@Autowired
	private UserService userService;
	@Autowired
	private TokenUtils tokenUtils;
	
	@GetMapping
	public ResponseEntity<List<ContactDTO>> getAllPage(Pageable page) {
		try {
			List<ContactDTO> dto = contactService.findAll(page).stream()
					.map(c -> new ContactDTO(c))
					.collect(Collectors.toList());
			return new ResponseEntity<List<ContactDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value="/all")
	public ResponseEntity<List<ContactDTO>> getAll(@RequestHeader("X-Auth-Token") String token) {
		try {
			List<ContactDTO> dto = userService.findByUsername(tokenUtils.getUsernameFromToken(token))
					.getContacts()
					.stream()
					.map(c -> new ContactDTO(c))
					.collect(Collectors.toList());
			return new ResponseEntity<List<ContactDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<ContactDTO> getOne(@PathVariable Long id) {
		try {
			Contact c = contactService.findOne(id);
			return new ResponseEntity<ContactDTO>(new ContactDTO(c), HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(consumes = { "multipart/form-data"})
	public ResponseEntity<ContactDTO> saveContact(@RequestParam("contact") String stringcontactDTO,
			@RequestPart MultipartFile file,
			@RequestHeader("X-Auth-Token") String token) {
		try {
			ContactDTO contactDTO = new ObjectMapper().readValue(stringcontactDTO, ContactDTO.class);
			User user = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
			if (user==null)
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			contactDTO.setId(null);
			Contact c = contactConverter.convert(contactDTO);
			c.setUser(user);
			user.addContact(c);
			userService.save(user);
			PhotoDTO photoDTO = contactService.savePhoto(file, c);
			ContactDTO dto = new ContactDTO(c);
			dto.setPhotoDTO(photoDTO);
			return new ResponseEntity<ContactDTO>(dto, HttpStatus.CREATED);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping(consumes = { "multipart/form-data"})
	public ResponseEntity<ContactDTO> updateContact(@RequestParam("contact") String stringcontactDTO,
			@RequestPart MultipartFile file,
			@RequestHeader("X-Auth-Token") String token) {
		try {
			ContactDTO contactDTO = new ObjectMapper().readValue(stringcontactDTO, ContactDTO.class);
			User user = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
			if (user==null)
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			Contact c = contactConverter.convert(contactDTO);
			c.setUser(user);
			user.addContact(c);
			userService.save(user);
			PhotoDTO photoDTO = contactService.updateContact(file, c, contactDTO.getPhotoDTO().getId());
			System.out.println(photoDTO.getFileDownloadUri());
			ContactDTO dto = new ContactDTO(c);
			dto.setPhotoDTO(photoDTO);
			return new ResponseEntity<ContactDTO>(dto, HttpStatus.ACCEPTED);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> removeContact (@PathVariable Long id) {
		try {
			contactService.remove(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
