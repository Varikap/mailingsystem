package com.project.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.converter.RuleConverter;
import com.project.dto.RuleDTO;
import com.project.model.Rule;
import com.project.service.RuleService;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value="api/rule")
@Log4j2
public class RuleController {
	@Autowired
	private RuleService ruleService;
	@Autowired
	private RuleConverter ruleConverter;
	
	@GetMapping
	public ResponseEntity<List<RuleDTO>> getAllPage(Pageable page) {
		try {
			List<RuleDTO> dto = ruleService.findAll(page).stream()
					.map(r -> new RuleDTO(r))
					.collect(Collectors.toList());
			return new ResponseEntity<List<RuleDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value="/all")
	public ResponseEntity<List<RuleDTO>> getAll() {
		try {
			List<RuleDTO> dto = ruleService.findAll().stream()
					.map(r -> new RuleDTO(r))
					.collect(Collectors.toList());
			return new ResponseEntity<List<RuleDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<RuleDTO> getOne (@PathVariable Long id) {
		try {
			Rule r = ruleService.findOne(id);
			return new ResponseEntity<RuleDTO>(new RuleDTO(r), HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(consumes="application/json")
	public ResponseEntity<RuleDTO> saveRule(@Valid @RequestBody RuleDTO ruleDTO) {
		try {
			ruleDTO.setId(null);
			Rule r = ruleService.save(ruleConverter.convert(ruleDTO));
			return new ResponseEntity<RuleDTO>(new RuleDTO(r), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PutMapping(value = "/{id}", consumes="application/json")
	public ResponseEntity<RuleDTO> updateRule (@Valid @RequestBody RuleDTO ruleDTO, @PathVariable Long id) {
		if (id != ruleDTO.getId())
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		try {
			Rule r = ruleService.save(ruleConverter.convert(ruleDTO));
			return new ResponseEntity<RuleDTO>(new RuleDTO(r), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> removeRule (@PathVariable Long id) {
		try {
			ruleService.remove(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
