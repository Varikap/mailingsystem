package com.project.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.converter.CreateUserConverter;
import com.project.converter.UserConverter;
import com.project.dto.CreateUserDTO;
import com.project.dto.UserDTO;
import com.project.model.User;
import com.project.service.UserService;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value="api/user")
@Log4j2
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private UserConverter userConverter;
	@Autowired
	private CreateUserConverter createUserConverter;

	@GetMapping
	public ResponseEntity<List<UserDTO>> getAllPage(Pageable page) {
		try {
			List<UserDTO> dto = userService.findAll(page).stream()
					.map(u -> new UserDTO(u))
					.collect(Collectors.toList());
			return new ResponseEntity<List<UserDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value="/all")
	public ResponseEntity<List<UserDTO>> getAll() {
		try {
			List<UserDTO> dto = userService.findAll().stream()
					.map(u -> new UserDTO(u))
					.collect(Collectors.toList());
			return new ResponseEntity<List<UserDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<UserDTO> getOne(@PathVariable Long id) {
		try {
			User u = userService.findOne(id);
			return new ResponseEntity<UserDTO>(new UserDTO(u), HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	@CrossOrigin
	@PostMapping(consumes="application/json")
	public ResponseEntity<CreateUserDTO> saveUser(@Valid @RequestBody CreateUserDTO userDTO) {
		try {
			User u = createUserConverter.convert(userDTO);
			u = userService.save(u);
			return new ResponseEntity<CreateUserDTO>(new CreateUserDTO(u), HttpStatus.CREATED);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PutMapping(value="/{id}", consumes="application/json")
	public ResponseEntity<UserDTO> updateUser(@Valid @RequestBody UserDTO userDTO, @PathVariable Long id){
		try {
			User u = userService.save(userConverter.convert(userDTO));
			return new ResponseEntity<UserDTO>(new UserDTO(u), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> removeUser(@PathVariable Long id) {
		try {
			userService.remove(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
