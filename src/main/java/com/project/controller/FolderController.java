package com.project.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.converter.FolderConverter;
import com.project.dto.FolderDTO;
import com.project.model.Folder;
import com.project.service.FolderService;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value = "api/folder")
@Log4j2
public class FolderController {
	@Autowired
	private FolderService folderService;
	@Autowired
	private FolderConverter folderConverter;
	
	@GetMapping
	public ResponseEntity<List<FolderDTO>> getAllPage(Pageable page) {
		try {
			List<FolderDTO> dto = folderService.findAll(page).stream()
					.map(f -> new FolderDTO(f))
					.collect(Collectors.toList());
			return new ResponseEntity<List<FolderDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value="/all")
	public ResponseEntity<List<FolderDTO>> getAll(){
		try {
			List<FolderDTO> dto = folderService.findAll().stream()
					.map(f -> new FolderDTO(f))
					.collect(Collectors.toList());
			return new ResponseEntity<List<FolderDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<FolderDTO> getOne(@PathVariable Long id) {
		try {
			Folder f = folderService.findOne(id);
			return new ResponseEntity<FolderDTO>(new FolderDTO(f), HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(consumes="application/json")
	public ResponseEntity<FolderDTO> saveFolder(@RequestBody FolderDTO folderDTO) {
		try {
			folderDTO.setId(null);
			Folder f = folderService.save(folderConverter.convert(folderDTO));
			return new ResponseEntity<FolderDTO>(new FolderDTO(f), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PutMapping(value = "/{id}", consumes="application/json")
	public ResponseEntity<FolderDTO> updateFolder (@PathVariable Long id, @RequestBody FolderDTO folderDTO){
		if (id != folderDTO.getId())
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		try {
			Folder f = folderService.save(folderConverter.convert(folderDTO));
			return new ResponseEntity<FolderDTO>(new FolderDTO(f), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> removeFolder (@PathVariable Long id){
		try {
			folderService.remove(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
