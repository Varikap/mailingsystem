package com.project.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.converter.MessageConverter;
import com.project.dto.MessageDTO;
import com.project.model.Account;
import com.project.model.Message;
import com.project.model.User;
import com.project.security.TokenUtils;
import com.project.service.EmailService;
import com.project.service.EmailServiceImpl;
import com.project.service.MessageService;
import com.project.service.UserService;

import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value="api/message")
@Log4j2
public class MessageController {
	@Autowired
	private MessageService messageService;
	@Autowired
	private MessageConverter messageConverter;
	@Autowired
	private EmailServiceImpl emailService;
	@Autowired
	private UserService userService;
	@Autowired
	private TokenUtils tokenUtils;
	
	@GetMapping(value="/all")
	public ResponseEntity<List<MessageDTO>> getAll(@RequestHeader("X-Auth-Token") String token,
			@RequestParam("folderName") String folderName) {
		try {
			User u = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
			List<MessageDTO> dto = emailService.downloadEmails(u, folderName);
			return new ResponseEntity<List<MessageDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			log.info("Something went wrong" + e.getMessage());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<MessageDTO> getOne(@PathVariable Long id){
		try {
			Message m = messageService.findOne(id);
			return new ResponseEntity<MessageDTO>(new MessageDTO(m), HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(consumes="application/json")
	public ResponseEntity<MessageDTO> saveMessage(@Valid @RequestBody MessageDTO messageDTO,
			@RequestHeader("X-Auth-Token") String token) {
		try {
			User user = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
			messageDTO.setId(null);
			Message m = emailService.sendMessage(messageDTO, user);
			return new ResponseEntity<MessageDTO>(new MessageDTO(m), HttpStatus.CREATED);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping(value="/attachment", consumes = { "multipart/form-data"})
	public ResponseEntity<MessageDTO> saveMessageWithAttachment(@RequestParam("message") String stringMessageDTO,
			@RequestHeader("X-Auth-Token") String token,
			@RequestPart(name="files", required = false) MultipartFile[] files) {
		try {
			MessageDTO messageDTO = new ObjectMapper().readValue(stringMessageDTO, MessageDTO.class);
			User user = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
			messageDTO.setId(null);
			Message m = emailService.sendMessageWithAttachment(messageDTO, user, files);
			return new ResponseEntity<MessageDTO>(new MessageDTO(m), HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	
	@PutMapping(value = "/{id}", consumes="application/json")
	public ResponseEntity<MessageDTO> updateMessage(@Valid @RequestBody MessageDTO messageDTO, @PathVariable Long id) {
		if (id != messageDTO.getId())
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		try {
			Message m = messageService.save(messageConverter.convert(messageDTO));
			return new ResponseEntity<MessageDTO>(new MessageDTO(m), HttpStatus.CREATED);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> removeMessage(@PathVariable Long id){
		try {
			messageService.remove(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
