package com.project.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.project.converter.AttachmentConverter;
import com.project.dto.AttachmentDTO;
import com.project.model.Attachment;
import com.project.service.AttachmentService;
import com.project.utils.MimeTypeConverter;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value = "api/attachment")
@Log4j2
public class AttachmentController {
	@Autowired
	private AttachmentService attachmentService;
	@Autowired
	private AttachmentConverter attachmentConverter;
	@Autowired
	private MimeTypeConverter mimeC;
	
	@GetMapping(value="/{id}")
	public ResponseEntity<Resource> getOne (@PathVariable Long id,HttpServletRequest request) {
		try {
			Attachment a = attachmentService.findOne(id);
			Resource resource = new ByteArrayResource(a.getData());
			var extension = mimeC.getDefaultExt(MediaType.parseMediaType(a.getMime_type()).toString());
			return ResponseEntity.ok()
	                .contentType(MediaType.parseMediaType(a.getMime_type()))
	                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + a.getName()+"."+extension + "\"")
	                .body(resource);
		} catch (Exception e) {
			log.info("id not found" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping
	public ResponseEntity<AttachmentDTO> saveAttachment(@RequestParam("file") MultipartFile file) {
		try {
			Attachment a = new Attachment(file.getBytes(),file.getContentType() ,file.getOriginalFilename());
			a = attachmentService.save(a);
			AttachmentDTO dto = new AttachmentDTO(a);
			dto.setSize(file.getSize());
			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
	                .path("api/attachment/")
	                .path(a.getId().toString())
	                .toUriString();
			dto.setFileDownloadUri(fileDownloadUri);
			dto.setName(file.getOriginalFilename());
			return new ResponseEntity<AttachmentDTO>(dto, HttpStatus.CREATED);
		} catch (Exception e) {
			log.info("Something went wrong" + e.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value = "/{id}", consumes="application/json")
	public ResponseEntity<AttachmentDTO> updateAttachment(@RequestBody AttachmentDTO attachmentDTO, @PathVariable Long id) {
		if (id != attachmentDTO.getId())
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		try {
			Attachment a = attachmentService.save(attachmentConverter.convert(attachmentDTO));
			return new ResponseEntity<AttachmentDTO>(new AttachmentDTO(a), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> removeAttachment (@PathVariable Long id) {
		try {
			attachmentService.remove(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}