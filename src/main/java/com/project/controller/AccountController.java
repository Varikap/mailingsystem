package com.project.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.converter.AccountConverter;
import com.project.dto.AccountDTO;
import com.project.model.Account;
import com.project.model.User;
import com.project.repository.UserRepository;
import com.project.security.TokenUtils;
import com.project.service.AccountService;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value="api/account")
@Log4j2
public class AccountController {
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private AccountConverter accountConverter;
	
	@Autowired
	private TokenUtils tokenUtils;
	
	@Autowired
	private UserRepository userRepo;
	
	@GetMapping
	public ResponseEntity<List<AccountDTO>> getAllPage(Pageable page) {
		try {
			List<AccountDTO> dto = accountService.findAll(page).stream()
					.map(a -> new AccountDTO())
					.collect(Collectors.toList());
			return new ResponseEntity<List<AccountDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<AccountDTO> getOne (@PathVariable Long id) {
		try {
			Account a = accountService.findOne(id);
			return new ResponseEntity<AccountDTO>(new AccountDTO(a), HttpStatus.OK);
		} catch (Exception e) {
			log.info("id not found");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value="/all")
	public ResponseEntity<List<AccountDTO>> getAll() {
		try {
			List<AccountDTO> dto = accountService.findAll().stream()
					.map(a -> new AccountDTO(a))
					.collect(Collectors.toList());
			return new ResponseEntity<List<AccountDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(consumes="application/json")
	public ResponseEntity<AccountDTO> saveAccount(@Valid @RequestBody AccountDTO accountDTO,
			@RequestHeader("X-Auth-Token") String token) {
		try {
			accountDTO.setId(null);
			Account account = accountService.save(accountConverter.convert(accountDTO), token);
			return new ResponseEntity<AccountDTO>(new AccountDTO(account), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value="hasAccount")
	public ResponseEntity<AccountDTO> accountExists(@RequestHeader("X-Auth-Token") String token){
		User u = userRepo.findByUsername(tokenUtils.getUsernameFromToken(token));
		try {
			var a = (Account) u.getAccounts().toArray()[0];
			if (a != null) {
				return new ResponseEntity<>(new AccountDTO(a), HttpStatus.NOT_FOUND);
			}
		}catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.OK);
		}
		return null;
	}
	
	@PutMapping(value = "/{id}", consumes="application/json")
	public ResponseEntity<AccountDTO> updateAccount(@Valid @RequestBody AccountDTO accountDTO, @PathVariable Long id) {
		if (id != accountDTO.getId())
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		try {
			Account a = accountService.save(accountConverter.convert(accountDTO));
			return new ResponseEntity<AccountDTO>(new AccountDTO(a), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}		
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> removeAccount (@PathVariable Long id) {
		try {
			accountService.remove(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}
