package com.project.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.converter.TagConverter;
import com.project.dto.TagDTO;
import com.project.model.Tag;
import com.project.service.TagService;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value="api/tag")
@Log4j2
public class TagController {
	@Autowired
	private TagService tagService;
	@Autowired
	private TagConverter tagConverter;
	
	@GetMapping
	public ResponseEntity<List<TagDTO>> getAllPage(Pageable page) {
		try {
			List<TagDTO> dto = tagService.findAll(page).stream()
					.map(t -> new TagDTO(t))
					.collect(Collectors.toList());
			return new ResponseEntity<List<TagDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value="/all")
	public ResponseEntity<List<TagDTO>> getAll() {
		try {
			List<TagDTO> dto = tagService.findAll().stream()
					.map(t -> new TagDTO(t))
					.collect(Collectors.toList());
			return new ResponseEntity<List<TagDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<TagDTO> getOne(@PathVariable Long id) {
		try {
			Tag t = tagService.findOne(id);
			return new ResponseEntity<TagDTO>(new TagDTO(t), HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(consumes="application/json")
	public ResponseEntity<TagDTO> saveTag (@Valid @RequestBody TagDTO tagDTO) {
		try {
			tagDTO.setId(null);
			Tag t = tagService.save(tagConverter.convert(tagDTO));
			return new ResponseEntity<TagDTO>(new TagDTO(t), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PutMapping(value="/{id}", consumes="application/json")
	public ResponseEntity<TagDTO> updateTag (@Valid @RequestBody TagDTO tagDTO, @PathVariable Long id) {
		if (id != tagDTO.getId())
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		try {
			Tag t = tagService.save(tagConverter.convert(tagDTO));
			return new ResponseEntity<TagDTO>(new TagDTO(t), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> removeTag(@PathVariable Long id) {
		try {
			tagService.remove(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
