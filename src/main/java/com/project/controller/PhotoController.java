package com.project.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.project.converter.PhotoConverter;
import com.project.dto.PhotoDTO;
import com.project.model.Contact;
import com.project.model.Photo;
import com.project.service.ContactService;
import com.project.service.FileStorageService;
import com.project.service.PhotoService;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value="api/photo")
@Log4j2
public class PhotoController {
	@Autowired
	private PhotoService photoService;
	@Autowired
	private PhotoConverter photoConverter;
	@Autowired
	private FileStorageService fileService;
	@Autowired
	private ContactService contactService;
	
	@GetMapping
	public ResponseEntity<List<PhotoDTO>> getAllPage(Pageable page) {
		try {
			List<PhotoDTO> dto = photoService.findAll(page).stream()
					.map(p -> new PhotoDTO(p))
					.collect(Collectors.toList());
			return new ResponseEntity<List<PhotoDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value="/all")
	public ResponseEntity<List<PhotoDTO>> getAll() {
		try {
			List<PhotoDTO> dto = photoService.findAll().stream()
					.map(p -> new PhotoDTO(p))
					.collect(Collectors.toList());
			return new ResponseEntity<List<PhotoDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<PhotoDTO> savePhoto(@PathVariable Long id) {
		try {
			Photo p = photoService.findOne(id);
			return new ResponseEntity<PhotoDTO>(new PhotoDTO(p), HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(consumes="multipart/form-data")
	public ResponseEntity<PhotoDTO> savePhoto (@RequestParam("file") MultipartFile file,
			@RequestParam("id") Long id) {
		try {
			Contact contact = contactService.findOne(id);
			String fileName = fileService.storeFile(file);
			Photo p = new Photo();
			p.setPath(fileName);
			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
	                .path("api/file/downloadFile/")
	                .path(fileName)
	                .toUriString();
			p = photoService.save(p);
			PhotoDTO dto = new PhotoDTO(p);
//			contact.addPhoto(p);
			contactService.save(contact);
			dto.setFileDownloadUri(fileDownloadUri);
			dto.setSize(file.getSize());
			return new ResponseEntity<PhotoDTO>(dto, HttpStatus.CREATED);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PutMapping(value="/{id}", consumes="application/json")
	public ResponseEntity<PhotoDTO> updatePhoto(@Valid @RequestBody PhotoDTO photoDTO, 
			@PathVariable Long id){
		if (id != photoDTO.getId())
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		try {
			Photo p = photoService.save(photoConverter.convert(photoDTO));
			return new ResponseEntity<PhotoDTO>(new PhotoDTO(p), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> removePhoto(@PathVariable Long id){
		try {
			photoService.remove(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			log.info("Something went wrong" + e.toString());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
