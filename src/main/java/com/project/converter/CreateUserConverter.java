package com.project.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.CreateUserDTO;
import com.project.model.User;

@Component
public class CreateUserConverter implements Converter<CreateUserDTO, User> {

	@Override
	public User convert(CreateUserDTO s) {
		User u = new User();
		u.setUsername(s.getUsername());
		u.setPassword(s.getPassword());
		u.setFirstname(s.getFirstname());
		u.setLastname(s.getLastname());
		return u;
	}
	
}
