package com.project.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.FolderDTO;
import com.project.model.Folder;

@Component
public class FolderConverter implements Converter<FolderDTO, Folder>{

	@Override
	public Folder convert(FolderDTO s) {
		Folder f = new Folder();
		f.setId(s.getId());
		f.setName(s.getName());
		return f;
	}

}
