package com.project.converter;

import java.util.Date;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.MessageDTO;
import com.project.model.Message;

@Component
public class MessageConverter implements Converter<MessageDTO, Message>{

	@Override
	public Message convert(MessageDTO s) {
		Message m = new Message();
		m.setId(s.getId());
		m.setFrom(s.getFrom());
		m.setTo(s.getTo());
		m.setCc(s.getCc());
		m.setBcc(s.getBcc());
		m.setDateTime(new Date());
		m.setSubject(s.getSubject());
		m.setContent(s.getContent());
		m.setUnread(s.getUnread());
		return m;
	}

}
