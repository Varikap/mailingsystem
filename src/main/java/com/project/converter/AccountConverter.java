package com.project.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.AccountDTO;
import com.project.model.Account;
import com.project.service.AccountService;

@Component
public class AccountConverter implements Converter<AccountDTO, Account>{
	@Autowired
	private AccountService service;

	@Override
	public Account convert(AccountDTO source) {
		Account a = new Account();
		a.setId(source.getId());
		a.setDisplayName(source.getDisplayName());
		a.setUsername(source.getUsername());
		a.setPassword(source.getPassword());
		a.setSmtpAddress(source.getSmtpAddress());
		a.setSmtpPort(source.getSmtpPort());
		a.setInServerAddress(source.getInServerAddress());
		a.setInServerType(source.getInServerType());
		return a;
	}

}
