package com.project.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.UserDTO;
import com.project.model.User;

@Component
public class UserConverter implements Converter<UserDTO, User>{

	@Override
	public User convert(UserDTO s) {
		User u = new User();
		u.setId(s.getId());
		u.setUsername(s.getUsername());
		u.setFirstname(s.getFirstname());
		u.setLastname(s.getLastname());
		return u;
	}

}
