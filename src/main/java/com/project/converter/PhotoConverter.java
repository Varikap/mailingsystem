package com.project.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.PhotoDTO;
import com.project.model.Photo;

@Component
public class PhotoConverter implements Converter<PhotoDTO, Photo>{

	@Override
	public Photo convert(PhotoDTO s) {
		Photo p = new Photo();
		p.setId(s.getId());
		return null;
	}

}
