package com.project.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.AttachmentDTO;
import com.project.model.Attachment;
import com.project.service.AttachmentService;

@Component
public class AttachmentConverter implements Converter<AttachmentDTO, Attachment> {

	@Override
	public Attachment convert(AttachmentDTO s) {
		Attachment a = new Attachment();
		a.setId(s.getId());
//		a.setData(s.getData());
		a.setMime_type(s.getMime_type());
		a.setName(s.getName());
		return a;
	}

}
