package com.project.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.RuleDTO;
import com.project.model.Rule;

@Component
public class RuleConverter implements Converter<RuleDTO, Rule>{

	@Override
	public Rule convert(RuleDTO s) {
		Rule r = new Rule();
		r.setId(s.getId());
		r.setValue(s.getValue());
		r.setCondition(s.getCondition());
		r.setOperation(s.getOperation());
		return null;
	}

}
