package com.project.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.TagDTO;
import com.project.model.Tag;

@Component
public class TagConverter implements Converter<TagDTO, Tag>{

	@Override
	public Tag convert(TagDTO s) {
		Tag t = new Tag();
		t.setId(s.getId());
		t.setName(s.getName());
		return t;
	}

}
