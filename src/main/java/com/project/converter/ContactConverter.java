package com.project.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.ContactDTO;
import com.project.model.Contact;

@Component
public class ContactConverter implements Converter<ContactDTO, Contact>{

	@Override
	public Contact convert(ContactDTO s) {
		Contact c = new Contact();
		c.setId(s.getId());
		c.setDisplayname(s.getDisplayname());
		c.setEmail(s.getEmail());
		c.setFirstname(s.getFirstname());
		c.setLastname(s.getLastname());
		c.setNote(s.getNote());
		return c;
	}

}
