package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.project.model.Tag;
import com.project.repository.TagRepository;

@Service
public class TagServiceImpl implements TagService{

	@Autowired
	TagRepository repo;
	
	@Override
	public Tag findOne(Long id) {
		return repo.findById(id).get();
	}

	@Override
	public List<Tag> findAll() {
		return repo.findAll();
	}

	@Override
	public Page<Tag> findAll(Pageable page) {
		return repo.findAll(page);
	}

	@Override
	public Tag save(Tag tag) {
		return repo.save(tag);
	}

	@Override
	public void remove(Long id) {
		repo.deleteById(id);
	}

}
