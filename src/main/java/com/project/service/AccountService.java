package com.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.project.model.Account;

public interface AccountService {
	Account findOne(Long id);
	
	List<Account> findAll();
	
	Page<Account> findAll(Pageable page);
	
	Account save(Account account);

	Account save(Account account, String token);
	
	void remove (Long id);
}
