package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.project.dto.PhotoDTO;
import com.project.model.Contact;
import com.project.model.Photo;
import com.project.repository.ContactRepository;

@Service
public class ContactServiceImpl implements ContactService{

	@Autowired
	private ContactRepository repo;
	
	@Autowired
	private FileStorageService fileService;
	
	@Autowired
	private PhotoService photoService;
	
	
	@Override
	public Contact findOne(Long id) {
		return repo.findById(id).get();
	}

	@Override
	public List<Contact> findAll() {
		return repo.findAll();
	}

	@Override
	public Page<Contact> findAll(Pageable page) {
		return repo.findAll(page);
	}

	@Override
	public Contact save(Contact contact) {
		return repo.save(contact);
	}

	@Override
	public void remove(Long id) {
		repo.deleteById(id);
	}

	@Override
	public PhotoDTO savePhoto(MultipartFile file, Contact c) {
		try {
			Contact contact = c;
			String fileName = fileService.storeFile(file);
			Photo p = new Photo();
			p.setPath(fileName);
			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
	                .path("api/file/downloadFile/")
	                .path(fileName)
	                .toUriString();
			contact.addPhoto(p);
			contact = this.save(contact);
			p.setContact(contact);
			p = photoService.save(p);
			PhotoDTO dto = new PhotoDTO(p);
			dto.setFileDownloadUri(fileDownloadUri);
			dto.setSize(file.getSize());
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public PhotoDTO updateContact(MultipartFile file, Contact c, Long photoID) {
		try {
			Contact contact = c;
			String fileName = fileService.storeFile(file);
			Photo p = photoService.findOne(photoID);
			p.setPath(fileName);
			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
	                .path("api/file/downloadFile/")
	                .path(fileName)
	                .toUriString();
			p.setContact(contact);
			p = photoService.save(p);
			contact.addPhoto(p);
			contact = this.save(contact);
			PhotoDTO dto = new PhotoDTO(p);
			dto.setFileDownloadUri(fileDownloadUri);
			dto.setSize(file.getSize());
			return dto;
		} catch (Exception e) {
			return null;
		}
	}
}
