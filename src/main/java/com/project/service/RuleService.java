package com.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.project.model.Rule;

public interface RuleService {
	Rule findOne(Long id);
	
	List<Rule> findAll();

	Page<Rule> findAll(Pageable page);
	
	Rule save (Rule rule);
	
	void remove (Long id);
}
