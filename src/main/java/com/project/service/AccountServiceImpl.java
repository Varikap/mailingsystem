package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.project.model.Account;
import com.project.model.User;
import com.project.repository.AccountRepository;
import com.project.repository.UserRepository;
import com.project.security.TokenUtils;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountRepository repo;
	@Autowired
	private TokenUtils tokenUtils;
	@Autowired
	private UserService userService;

	@Override
	public Account findOne(Long id) {
		return repo.findById(id).get();
	}

	@Override
	public List<Account> findAll() {
		return repo.findAll();
	}

	@Override
	public Page<Account> findAll(Pageable page) {
		return repo.findAll(page);
	}

	@Override
	public Account save(Account account) {
		return repo.save(account);
	}
	
	@Override
	public Account save(Account account, String token) {
		User user = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
		account.setUser(user);
		return repo.save(account);
	}

	@Override
	public void remove(Long id) {
		repo.deleteById(id);
	}

}
