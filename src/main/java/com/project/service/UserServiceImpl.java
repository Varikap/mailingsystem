package com.project.service;

import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.project.model.Authority;
import com.project.model.User;
import com.project.model.UserAuthority;
import com.project.model.enumerations.Role;
import com.project.repository.AuthorityRepository;
import com.project.repository.UserAuthorityRepository;
import com.project.repository.UserRepository;

@Service
public class UserServiceImpl  implements UserService{

	@Autowired
	UserRepository userRepo;
	
	@Autowired
	private UserAuthorityRepository userAuthRepo;
	
	@Autowired
	private AuthorityRepository authRepo;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public User findOne(Long id) {
		return userRepo.findById(id).get();
	}

	@Override
	public List<User> findAll() {
		return userRepo.findAll();
	}

	@Override
	public Page<User> findAll(Pageable page) {
		return userRepo.findAll(page);
	}

	@Override
	public User save(User user) {
		return userRepo.save(this.save(user.getUsername(),user.getPassword(),Role.USER, user));
	}

	@Override
	public void remove(Long id) {
		userRepo.deleteById(id);
	}

	@Override
	public User save(String username, String password, Role role, User u) {
		u.setUsername(username);
		u.setPassword(passwordEncoder.encode(password));
		Authority auth = authRepo.getOneByRole(role);
		UserAuthority uauth = new UserAuthority();
		uauth.setUser(u);
		uauth.setAuthority(auth);
		HashSet<UserAuthority> set = new HashSet<>();
		set.add(uauth);
		u.setUserAuthorities(set);
		userAuthRepo.save(uauth);
		return u;
	}

	@Override
	public User findByUsername(String username) {
		return userRepo.findByUsername(username);
	}

}
