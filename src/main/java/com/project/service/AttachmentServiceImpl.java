package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.project.model.Attachment;
import com.project.repository.AttachmentRepository;

@Service
public class AttachmentServiceImpl implements AttachmentService{


	@Autowired
	AttachmentRepository repo;
	
	@Override
	public Attachment findOne(Long id) {
		return repo.findById(id).get();
	}

	@Override
	public List<Attachment> findAll() {
		return repo.findAll();
	}

	@Override
	public Page<Attachment> findAll(Pageable page) {
		return repo.findAll(page);
	}

	@Override
	public Attachment save(Attachment att) {
		return repo.save(att);
	}

	@Override
	public void remove(Long id) {
		repo.deleteById(id);
	}
}
