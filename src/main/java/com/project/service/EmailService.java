package com.project.service;

import javax.mail.MessagingException;

import com.project.model.Message;

public interface EmailService {
	public void sendMessage(Message messageInfo) throws MessagingException;
}
