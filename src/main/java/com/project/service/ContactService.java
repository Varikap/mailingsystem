package com.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import com.project.dto.PhotoDTO;
import com.project.model.Contact;

public interface ContactService {
	Contact findOne (Long id);
	
	List<Contact> findAll();
	
	Page<Contact> findAll(Pageable page);
	
	Contact save (Contact contact);
	
	void remove (Long id);
	
	PhotoDTO savePhoto(MultipartFile file, Contact contact);
	
	PhotoDTO updateContact(MultipartFile file, Contact contact, Long photoID);
}
