package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.project.model.Photo;
import com.project.repository.PhotoRepository;

@Service
public class PhotoServiceImpl implements PhotoService {

	@Autowired
	PhotoRepository repo;
	
	@Override
	public Photo findOne(Long id) {
		return repo.findById(id).get();
	}

	@Override
	public List<Photo> findAll() {
		return repo.findAll();
	}

	@Override
	public Page<Photo> findAll(Pageable page) {
		return repo.findAll(page);
	}

	@Override
	public Photo save(Photo photo) {
		return repo.save(photo);
	}

	@Override
	public void remove(Long id) {
		repo.deleteById(id);
	}

}
