package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.project.model.Folder;
import com.project.repository.FolderRepository;

@Service
public class FolderServiceImpl implements FolderService{

	@Autowired
	FolderRepository repo;
	
	@Override
	public Folder findOne(Long id) {
		return repo.findById(id).get();
	}

	@Override
	public List<Folder> findAll() {
		return repo.findAll();
	}

	@Override
	public Page<Folder> findAll(Pageable page) {
		return repo.findAll(page);
	}

	@Override
	public Folder save(Folder folder) {
		return repo.save(folder);
	}

	@Override
	public void remove(Long id) {
		repo.deleteById(id);
	}

}
