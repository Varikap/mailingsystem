package com.project.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.Properties;
import java.util.Random;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.Flags.Flag;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.project.converter.MessageConverter;
import com.project.dto.AttachmentDTO;
import com.project.dto.MessageDTO;
import com.project.model.Account;
import com.project.model.Attachment;
import com.project.model.User;
import com.project.utils.FolderNameConverter;

//TODO DODAJ IMPLEMENTACIJU MAIL I PROMENI DA SVI AUTOWIRE INTERFACE PLS

@Service
public class EmailServiceImpl{
	
	@Autowired
	private MessageService messageService;
	@Autowired
	private MessageConverter messageConverter;
	@Autowired
	private AttachmentService attachmentService;
	@Autowired
	private FolderNameConverter folderNameConverter;
	
//	@Override
	public com.project.model.Message sendMessage(MessageDTO messageInfo, User user) throws MessagingException {
	      String to = messageInfo.getTo();
	      Account account = (Account) user.getAccounts().toArray()[0];
	      String from = account.getUsername();
	      final String username = from.split("@")[0];
	      final String password = account.getPassword();
	      String host = account.getSmtpAddress();

	      Properties props = new Properties();
	      props.put("mail.smtp.auth", "true");
	      props.put("mail.smtp.ssl.enable", "true"); 
	      props.put("mail.smtp.host", host);
	      props.put("mail.smtp.port", account.getSmtpPort().toString());

	      Session session = Session.getInstance(props,
	         new javax.mail.Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	               return new PasswordAuthentication(username, password);
	    }
	         });

	    try {
	    MimeMessage message = new MimeMessage(session);
	    message.setFrom(new InternetAddress(from));
	    message.setRecipients(javax.mail.Message.RecipientType.TO,
	               InternetAddress.parse(to));
	    
	    if(messageInfo.getCc() == null || messageInfo.getCc().equals("")|| messageInfo.getCc().equals("''")) {} 
	    else {
	    	message.setRecipient(javax.mail.Message.RecipientType.CC, 
	    			new InternetAddress(messageInfo.getCc()));
	    }
	    if(messageInfo.getBcc() == null || messageInfo.getBcc().equals("") || messageInfo.getBcc().equals("''")){}
	    else {
	    	message.setRecipient(javax.mail.Message.RecipientType.BCC, 
	    			new InternetAddress(messageInfo.getBcc()));
	    }
	    message.setSubject(messageInfo.getSubject());
	    message.setText(messageInfo.getContent());
	    Transport.send(message);
	    com.project.model.Message m = messageConverter.convert(messageInfo);
	    m.setAccount(account);
	    m.setFrom(from);
	    return messageService.save(m);
		    
	      } catch (MessagingException e) {
	         throw new RuntimeException(e);
	      } catch (Exception e) {
			System.out.println(e.getStackTrace());
			return null;
	      }	
	}
	
	public com.project.model.Message sendMessageWithAttachment(MessageDTO messageInfo,
			User user, MultipartFile[] files) throws IOException {
		
		  String to = messageInfo.getTo();
	      Account account = (Account) user.getAccounts().toArray()[0];
	      String from = account.getUsername();
	      final String username = from.split("@")[0];
	      final String password = account.getPassword();
	      String host = account.getSmtpAddress();
	
	      Properties props = new Properties();
	      props.put("mail.smtp.auth", "true");
	      props.put("mail.smtp.ssl.enable", "true"); 
	      props.put("mail.smtp.host", host);
	      props.put("mail.smtp.port", account.getSmtpPort().toString());
	
	      Session session = Session.getInstance(props,
	         new javax.mail.Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	               return new PasswordAuthentication(username, password);
	            }
	         });
	      try {
	          Message message = new MimeMessage(session);
	          message.setFrom(new InternetAddress(from));
	          message.setRecipients(Message.RecipientType.TO,
	             InternetAddress.parse(to));
	          message.setSubject(messageInfo.getSubject());
	          if(messageInfo.getCc() == null || messageInfo.getCc() == "" || messageInfo.getCc() == "''") {} 
	  	      else {
	  	    	message.setRecipient(javax.mail.Message.RecipientType.CC, 
	  	    			new InternetAddress(messageInfo.getCc()));
		  	  }
		  	  if(messageInfo.getBcc() == null || messageInfo.getBcc() == "" || messageInfo.getBcc() == "''"){}
		  	  else {
		  	    message.setRecipient(javax.mail.Message.RecipientType.BCC, 
		  	    			new InternetAddress(messageInfo.getBcc()));
		  	   }
	          BodyPart messageBodyPart = new MimeBodyPart();
	          messageBodyPart.setText(messageInfo.getContent());
	          Multipart multipart = new MimeMultipart();
	          Set<Attachment> attachments = new HashSet<>();
	          for(int i = 0;i< files.length; i++) {
	        	  
		          
		          multipart.addBodyPart(messageBodyPart);
		          messageBodyPart = new MimeBodyPart();
	        	  
	        	  messageBodyPart.setFileName(files[i].getOriginalFilename());
	        	  System.out.println(files[i].getOriginalFilename());
		          messageBodyPart.setContent(files[i].getBytes(), files[i].getContentType());
		          Attachment a = new Attachment(files[i].getBytes(), files[i].getContentType(), files[i].getName());
		          attachments.add(a);
		          messageBodyPart.setDisposition(Part.ATTACHMENT);
		          multipart.addBodyPart(messageBodyPart);
		          message.setContent(multipart);
	          }
	          Transport.send(message);
	          com.project.model.Message m = messageConverter.convert(messageInfo);
	          m.setAccount(account);
	          m.setFrom(from);
	          m.addAttachments(attachments);
	          System.out.println("attach");
	          m = messageService.save(m);
	          for(Attachment attachment : attachments) {
	        	  attachment.setMessage(m);
	        	  attachment = attachmentService.save(attachment);
	          }

	          System.out.println("Poslao");
	          return m;
	          
	       } catch (MessagingException e) {
	          throw new RuntimeException(e);
	       } catch (Exception e) {
	    	   System.out.println(e.getStackTrace());
		}
	      
	      return null;
	}	
	
	private Properties getServerProperties(String protocol, String host,
            String port) {
        Properties properties = new Properties();
 
        properties.put(String.format("mail.%s.host", protocol), host);
        properties.put(String.format("mail.%s.port", protocol), port);
 
        properties.setProperty(
                String.format("mail.%s.socketFactory.class", protocol),
                "javax.net.ssl.SSLSocketFactory");
        properties.setProperty(
                String.format("mail.%s.socketFactory.fallback", protocol),
                "false");
        properties.setProperty(
                String.format("mail.%s.socketFactory.port", protocol),
                String.valueOf(port));
 
        return properties;
    }
 
    public List<MessageDTO> downloadEmails(User u, String folderName) {
    	folderName = folderNameConverter.convert(folderName);
    	var a = (Account) u.getAccounts().toArray()[0];
    	String protocol = "imaps";
    	String host = a.getInServerAddress();
    	String port = String.valueOf(a.getInServerPort());
        Properties properties = getServerProperties(protocol, host, port);
        Session session = Session.getDefaultInstance(properties);
        try {
            Store store = session.getStore(protocol);

  	        final String username = a.getUsername().split("@")[0];
  	        final String password = a.getPassword();
            store.connect(username, password);;
            Folder folderInbox = store.getFolder(folderName);
            folderInbox.open(Folder.READ_ONLY);
            javax.mail.Message[] messages = folderInbox.getMessages();
            List<MessageDTO> dtoList = new ArrayList<>();
            for (int i = 0; i < messages.length; i++) {
            	MessageDTO dto = new MessageDTO();
                Message msg = messages[i];
                Address[] fromAddress = msg.getFrom();
                String from = fromAddress[0].toString();
                String subject = msg.getSubject();
                String toList = parseAddresses(msg
                        .getRecipients(RecipientType.TO));
                String ccList = parseAddresses(msg
                        .getRecipients(RecipientType.CC));
                String sentDate = msg.getSentDate().toString();
 
                String contentType = msg.getContentType();
                String messageContent = "";
                List<AttachmentDTO> filesList= new ArrayList<>();
                if (contentType.contains("TEXT/PLAIN")
                        || contentType.contains("TEXT/HTML")) {
                    try {
                        Object content = msg.getContent();
                        if (content != null) {
                            messageContent = content.toString();
                        }
                    } catch (Exception ex) {
                        messageContent = "[Error downloading content]";
                        ex.printStackTrace();
                    }
                } else if(contentType.contains("multipart")) {
                	var files = (Multipart) msg.getContent();
                	for (int j=0; j<files.getCount();j++) {
                		
                		BodyPart bodyPart = files.getBodyPart(j);
                		if (bodyPart.isMimeType("text/*") || bodyPart.isMimeType("multipart/*") 
                				|| bodyPart.isMimeType("multipart/alternative")) {
	                		messageContent =  this.getText(bodyPart);
                		}
	                		String generatedString = UUID.randomUUID().toString();
                        
                        InputStream is = bodyPart.getInputStream();
                        
                        Attachment attachment = new Attachment(is.readAllBytes(),bodyPart.getContentType().split(";")[0] ,generatedString);
                        attachment = attachmentService.save(attachment);
                        AttachmentDTO dtoAttachment = new AttachmentDTO(attachment);
                        dtoAttachment.setSize(new Long(bodyPart.getSize()));
            			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
            	                .path("api/attachment/")
            	                .path(attachment.getId().toString())
            	                .toUriString();
            			dtoAttachment.setFileDownloadUri(fileDownloadUri);
            			dtoAttachment.setName(generatedString);
            			filesList.add(dtoAttachment);
                	}
                }
                
                dto.setFrom(from);
                dto.setTo(toList);
                dto.setCc(ccList);
                dto.setSubject(subject);
                dto.setDateTime(sentDate);
                dto.setContent(messageContent);
                dto.setUnread(!msg.isSet(Flag.SEEN));
                dto.setFiles(filesList);
                dto.setId(new Long(i));
                dtoList.add(dto);
            }
 
            folderInbox.close(false);
            store.close();
            return dtoList;
        } catch (NoSuchProviderException ex) {
            System.out.println("No provider for protocol: " + protocol);
            ex.printStackTrace();
        } catch (MessagingException ex) {
            System.out.println("Could not connect to the message store");
            ex.printStackTrace();
        } catch (IOException ex) {
			ex.printStackTrace();
		}
        return null;
    }

    private String parseAddresses(Address[] address) {
        String listAddress = "";
 
        if (address != null) {
            for (int i = 0; i < address.length; i++) {
                listAddress += address[i].toString() + ", ";
            }
        }
        if (listAddress.length() > 1) {
            listAddress = listAddress.substring(0, listAddress.length() - 2);
        }
 
        return listAddress;
    }
	
    private String getText(Part p) throws MessagingException, IOException {
    	Boolean textIsHtml = false;
		if (p.isMimeType("text/*")) {
			String s = (String)p.getContent();
			textIsHtml = p.isMimeType("text/html");
			return s;
		}
		
		if (p.isMimeType("multipart/alternative")) {
			Multipart mp = (Multipart)p.getContent();
			String text = null;
			for (int i = 0; i < mp.getCount(); i++) {
			    Part bp = mp.getBodyPart(i);
			    if (bp.isMimeType("text/plain")) {
			        if (text == null)
			            text = getText(bp);
			        continue;
			    } else if (bp.isMimeType("text/html")) {
			        String s = getText(bp);
			        if (s != null)
			            return s;
			    } else {
			        return getText(bp);
			    }
			}
		} else if (p.isMimeType("multipart/*")) {
			Multipart mp = (Multipart)p.getContent();
			for (int i = 0; i < mp.getCount(); i++) {
			    String s = getText(mp.getBodyPart(i));
			    if (s != null)
			        return s;
				}
		}
		
		return null;
    }	
    
}
