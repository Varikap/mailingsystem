package com.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.project.model.Message;

public interface MessageService {
	Message findOne (Long id);
	
	List<Message> findAll();
	
	Page<Message> findAll(Pageable page);
	
	Message save (Message message);
	
	void remove (Long id);

}
