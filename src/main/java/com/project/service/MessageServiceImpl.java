package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.project.model.Message;
import com.project.repository.MessageRepository;

@Service
public class MessageServiceImpl implements MessageService{

	@Autowired
	MessageRepository repo;
	
	@Override
	public Message findOne(Long id) {
		return repo.findById(id).get();
	}

	@Override
	public List<Message> findAll() {
		return repo.findAll();
	}

	@Override
	public Page<Message> findAll(Pageable page) {
		return repo.findAll(page);
	}

	@Override
	public Message save(Message message) {
		return repo.save(message);
	}

	@Override
	public void remove(Long id) {
		repo.deleteById(id);
	}

}
