package com.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.project.model.User;
import com.project.model.enumerations.Role;

public interface UserService {
	User findOne (Long id);
	
	List<User> findAll();
	
	Page<User> findAll(Pageable page);
	
	User save (User user);
	
	void remove (Long id);
	
	User save (String username, String password, Role role, User u);
	
	User findByUsername(String username);
}
