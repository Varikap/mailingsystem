package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.project.model.Rule;
import com.project.repository.RuleRepository;

@Service
public class RuleServiceImpl implements RuleService{

	@Autowired
	RuleRepository repo;
	
	@Override
	public Rule findOne(Long id) {
		return repo.findById(id).get();
	}

	@Override
	public List<Rule> findAll() {
		return repo.findAll();
	}

	@Override
	public Page<Rule> findAll(Pageable page) {
		return repo.findAll(page);
	}

	@Override
	public Rule save(Rule rule) {
		return repo.save(rule);
	}

	@Override
	public void remove(Long id) {
		repo.deleteById(id);
	}

}
