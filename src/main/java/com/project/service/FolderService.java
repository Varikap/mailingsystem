package com.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.project.model.Folder;

public interface FolderService {
	Folder findOne (Long id);
	
	List<Folder> findAll();
	
	Page<Folder> findAll(Pageable page);
	
	Folder save(Folder folder);
	
	void remove (Long id);
}
