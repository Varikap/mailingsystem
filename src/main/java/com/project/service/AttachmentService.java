package com.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.project.model.Attachment;

public interface AttachmentService {
	Attachment findOne (Long id);
	
	List<Attachment> findAll();
	
	Page<Attachment> findAll(Pageable page);
	
	Attachment save(Attachment att);
	
	void remove (Long id);
}
