package com.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.project.model.Photo;

public interface PhotoService {
	Photo findOne (Long id);
	
	List<Photo> findAll();
	
	Page<Photo> findAll(Pageable page);
	
	Photo save (Photo photo);
	
	void remove (Long id);
}
