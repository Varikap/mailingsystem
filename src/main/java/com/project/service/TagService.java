package com.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.project.model.Tag;

public interface TagService {
	Tag findOne (Long id);

	List<Tag> findAll();
	
	Page<Tag> findAll(Pageable page);
	
	Tag save (Tag tag);
	
	void remove (Long id);
}
