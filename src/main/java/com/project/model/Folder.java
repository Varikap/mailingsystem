package com.project.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "folders")
@Getter @Setter
@NoArgsConstructor
public class Folder {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Long id;
	@Column(columnDefinition = "VARCHAR(100)")
	private String name;
	@OneToMany(mappedBy="parentFolder")
	private Set<Folder> subFolders = new HashSet<Folder>();
	@ManyToOne
	private Folder parentFolder;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="account_id")
	private Account account;
	@OneToMany(mappedBy="folder",fetch = FetchType.LAZY, cascade =CascadeType.PERSIST)
	private Set<Rule> rules = new HashSet<Rule>();
	@OneToMany(mappedBy="folder",fetch = FetchType.LAZY, cascade =CascadeType.PERSIST)
	private Set<Message> messages = new HashSet<Message>();
	
	public Folder(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public void addRule(Rule r) {
		this.rules.add(r);
	}
	
	public void removeRule(Rule r) {
		this.rules.remove(r);
	}
	
	public void addMessage(Message m) {
		this.messages.add(m);
	}
	
	public void removeMessage(Message m) {
		this.messages.remove(m);
	}
	
	public void addFolder(Folder f) {
		this.subFolders.add(f);
	}
	
	public void removeFolder(Folder f) {
		this.subFolders.remove(f);
	}

}
