package com.project.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Table(name = "accounts")
@Getter @Setter
@NoArgsConstructor
public class Account {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Long id;
	@Column(columnDefinition = "VARCHAR(20)")
	private String username;
	@Column(columnDefinition = "VARCHAR(20)")
	private String password;
	@Column(columnDefinition = "VARCHAR(100)")
	private String displayName;
	@Column(columnDefinition = "VARCHAR(250)")
	private String smtpAddress;
	@Column
	private Integer smtpPort;
	@Column(columnDefinition = "VARCHAR(250)")
	private String inServerAddress;
	@Column
	private Integer inServerType;
	@Column
	private Integer inServerPort;
	@OneToMany(mappedBy="account", fetch = FetchType.LAZY, cascade =CascadeType.REMOVE)
	private Set<Folder> folders = new HashSet<Folder>();
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
	private User user;
	@OneToMany(mappedBy="account",fetch = FetchType.LAZY, cascade =CascadeType.REMOVE)
	private Set<Message> messages = new HashSet<Message>();
	
	public Account(Long id, String username, String password, String displayName, String smtpAddress, Integer smtpPort,
			String inServerAddress, Integer inServerType, Integer inServerPort) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.displayName = displayName;
		this.smtpAddress = smtpAddress;
		this.smtpPort = smtpPort;
		this.inServerAddress = inServerAddress;
		this.inServerType = inServerType;
		this.inServerPort = inServerPort;
	}
		
	public void addFolder(Folder f) {
		this.folders.add(f);
	}
	
	public void removeFolder(Folder f) {
		this.folders.remove(f);
	}
	
	public void addMessage(Message m) {
		this.messages.add(m);
	}
	
	public void removeMessage(Message m) {
		this.messages.remove(m);
	}

}
