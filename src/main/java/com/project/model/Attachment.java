package com.project.model;


import javax.persistence.*;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "attachments")
@Getter @Setter
@NoArgsConstructor
public class Attachment {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Long id;
	@Column @Lob
	private byte[]  data;
	@Column(columnDefinition = "VARCHAR(200)")
	private String mime_type;
	@Column(columnDefinition = "VARCHAR(100)")
	private String name;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="message_id")
	private Message message;
	public Attachment(byte[] data, String mime_type, String name) {
		super();
		this.data = data;
		this.mime_type = mime_type;
		this.name = name;
	}
	
	
}
