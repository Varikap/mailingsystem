package com.project.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "messages")
@Getter @Setter
@NoArgsConstructor
public class Message {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Long id;
	@Column(name="valueFrom", columnDefinition = "VARCHAR(100)") 
	private String from;
	@Column(name="valueTo", columnDefinition = "TEXT")
	private String to;
	@Column(columnDefinition = "TEXT")
	private String cc;
	@Column(columnDefinition = "TEXT")
	private String bcc;
	@Column
	private Date dateTime;
	@Column(columnDefinition = "VARCHAR(250)")
	private String subject;
	@Column(columnDefinition = "TEXT")
	private String content;
	@Column
	private Boolean unread;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="folder_id")
	private Folder folder;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="account_id")
	private Account account;
	@OneToMany(mappedBy="message", fetch=FetchType.LAZY, cascade=CascadeType.REMOVE)
	private Set<Attachment> attachments = new HashSet<Attachment>();
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(
		name="messages_tags",
		joinColumns=@JoinColumn(name="message_id"),
		inverseJoinColumns=@JoinColumn(name="tag_id"))
	private Set<Tag> tags;
	
	public Message(Long id, String from, String to, String cc, String bcc, Date dateTime, String subject,
			String content, Boolean unread) {
		super();
		this.id = id;
		this.from = from;
		this.to = to;
		this.cc = cc;
		this.bcc = bcc;
		this.dateTime = dateTime;
		this.subject = subject;
		this.content = content;
		this.unread = unread;
	}
	
	public void addAttachment(Attachment a) {
		this.attachments.add(a);
	}
	public void addAttachments(Set<Attachment> a) {
		for (Attachment attachment : a) {
			this.attachments.add(attachment);
		}
	}
	
	public void removeAttachment(Attachment a) {
		this.attachments.remove(a);
	}
	
	public void addTag(Tag t) {
		this.tags.add(t);
	}

	public void removeTag(Tag t) {
		this.tags.remove(t);
	}

	
}
