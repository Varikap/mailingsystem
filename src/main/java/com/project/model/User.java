package com.project.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table
@Getter @Setter
@NoArgsConstructor
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_id")
	private Long id;
	@Column(columnDefinition = "VARCHAR(20)")
	private String username;
	@Column(columnDefinition = "VARCHAR(200)")
	private String password;
	@Column(columnDefinition = "VARCHAR(100)")
	private String firstname;
	@Column(columnDefinition = "VARCHAR(100)")
	private String lastname;
	@OneToMany(mappedBy="user", fetch=FetchType.LAZY, cascade=CascadeType.REMOVE)
	private Set<Tag> tags = new HashSet<Tag>();
	@OneToMany(mappedBy="user", fetch = FetchType.LAZY)
	private Set<Contact> contacts = new HashSet<Contact>();
	@OneToMany(mappedBy="user", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Set<Account> accounts = new HashSet<Account>();
	@OneToMany(mappedBy="user", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<UserAuthority> userAuthorities = new HashSet<UserAuthority>();

	public User(Long id, String username, String password, String firstname, String lastname) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
	}
	
	public void addContact(Contact c) {
		this.contacts.add(c);
	}
	
	public void removeContact(Contact c) {
		this.contacts.remove(c);
	}
	
	public void addTag(Tag t) {
		this.tags.add(t);
	}
	
	public void removeTag(Tag t) {
		this.tags.remove(t);
	}
	
	public void addAccounts(Account a) {
		this.accounts.add(a);
	}
	
	public void removeAccount(Account a) {
		this.accounts.remove(a);
	}

}
