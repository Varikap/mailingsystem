package com.project.model;


import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "rules")
@Getter @Setter
@NoArgsConstructor
public class Rule {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Long id;
	@Column(name="valueValue", columnDefinition = "VARCHAR(250)")
	private String value;
	@Column(name="valueCondition")
	private Integer condition;
	@Column(name="valueOperation")
	private Integer operation;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="folder_id")
	private Folder folder;
	public Rule(Long id, String value, Integer condition, Integer operation) {
		super();
		this.id = id;
		this.value = value;
		this.condition = condition;
		this.operation = operation;
	}
}
