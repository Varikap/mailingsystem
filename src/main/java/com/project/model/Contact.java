package com.project.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "contacts")
@Getter @Setter
@NoArgsConstructor
public class Contact {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Long id;
	@Column(columnDefinition = "VARCHAR(100)")
	private String displayname;
	@Column(columnDefinition = "VARCHAR(100)")
	private String email;
	@Column(columnDefinition = "VARCHAR(100)")
	private String firstname;
	@Column(columnDefinition = "VARCHAR(100)")
	private String lastname;
	@Column(columnDefinition = "TEXT")
	private String note;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
	private User user;
	@OneToMany(mappedBy="contact")
	private Set<Photo> photos = new HashSet<Photo>();

	public Contact(Long id, String displayname, String email, String firstname, String lastname, String note) {
		super();
		this.id = id;
		this.displayname = displayname;
		this.email = email;
		this.firstname = firstname;
		this.lastname = lastname;
		this.note = note;
	}
	
	public void addPhoto(Photo p) {
		this.photos.add(p);
	}
	
	public void removePhoto(Photo p) {
		this.photos.remove(p);
	}

	
}
