package com.project.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tags")
@Getter @Setter
@NoArgsConstructor
public class Tag {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Long id;
	@Column(columnDefinition = "VARCHAR(250)")
	private String name;
	@ManyToMany(mappedBy="tags")
	private Set<Message> messages = new HashSet<Message>();
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
	private User user;

	public Tag(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public void addMessage(Message m) {
		this.messages.add(m);
	}
	
	public void removeMessage(Message m) {
		this.messages.remove(m);
	}

}
