package com.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.model.Folder;

@Repository
public interface FolderRepository extends JpaRepository<Folder, Long> {

}
