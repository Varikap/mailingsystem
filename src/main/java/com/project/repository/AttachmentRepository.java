package com.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.model.Attachment;

@Repository
public interface AttachmentRepository extends JpaRepository<Attachment, Long>{

}
