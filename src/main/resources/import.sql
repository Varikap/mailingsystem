insert into authority (role) values (0);
insert into authority (role) values (1);

-- password is 'admin' (bcrypt encoded) 
insert into user (username, password) values ('admin', '$2a$04$SwzgBrIJZhfnzOw7KFcdzOTiY6EFVwIpG7fkF/D1w26G1.fWsi.aK');


-- password is 'user' (bcrypt encoded)
insert into user (username, password) values ('user', '$2a$04$Amda.Gm4Q.ZbXz9wcohDHOhOBaNQAkSS1QO26Eh8Hovu3uzEpQvcq');

-- password is 'string' (bcrypt encoded)
insert into user (username, password, firstname, lastname) values ('string', '$2a$04$ZU6a2iXqMTaiQRJvPkIud.FsY7adF9y4QdZicamtebme5jwFPK.ji', 'stringg', 'string');


insert into user_authority (user_user_id, authority_id) values (1, 1);
insert into user_authority (user_user_id, authority_id) values (2, 2);
insert into user_authority (user_user_id, authority_id) values (3, 2);

-- account for user
-- insert into accounts (display_name, in_server_address, in_server_port, in_server_type, password, smtp_address, smtp_port, username, user_id) values ('string', 'pop.gmail.com', 995, 0, 'neznamjakako', 'smtp.gmail.com', 465, 'osumejl97@gmail.com', 3); 

insert into accounts (display_name, in_server_address, in_server_port, in_server_type, password, smtp_address, smtp_port, username, user_id) values ('string', 'imap.gmail.com', 993, 0, 'neznamjakako', 'smtp.gmail.com', 465, 'osumejl97@gmail.com', 3);